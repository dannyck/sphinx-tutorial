# Sphinx

#tech/python/sphinx
#edit

## Reference
* [Documentation — The Hitchhiker’s Guide to Python](http://docs.python-guide.org/en/latest/writing/documentation/)
* [Overview — Sphinx 1.8.0+ documentation](http://www.sphinx-doc.org/en/master/)
* [reStructuredText — Useful Syntax](https://goo.gl/3umEMX)

## Installation


### 1. Install necessary modules

`pip install sphinx sphinx_bootstrap_theme`

- [sphinx](http://www.sphinx-doc.org/en/master/)

- [sphinx bootstrap theme](https://github.com/ryan-roemer/sphinx-bootstrap-theme)

### 2. Init Project

Given the project like this:

`mkdir src release docs`
`touch ReadMe.md`

```
/
|- docs
|- src
|- release
|- ReadMe.md
```

- `docs` : API documentation
- `src`: source code
- `release`

RUN `sphinx-quickstart` in **docs**

```
/
|- docs
	 |- build
	 |- source
|- src
|- release
|- ReadMe.md
```

Start-up config:
```
Welcome to the Sphinx 1.7.1 quickstart utility.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.
> Separate source and build directories (y/n) [n]: y

Inside the root directory, two more directories will be created; "_templates"
for custom HTML templates and "_static" for custom stylesheets and other static
files. You can enter another prefix (such as ".") to replace the underscore.
> Name prefix for templates and static dir [_]: 

The project name will occur in several places in the built documentation.
> Project name: Sphinx-Tutorial
> Author name(s): Danny Chan
> Project release []: 

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see
http://sphinx-doc.org/config.html#confval-language.
> Project language [en]: 

The file name suffix for source files. Commonly, this is either ".txt"
or ".rst".  Only files with this suffix are considered documents.
> Source file suffix [.rst]: 

One document is special in that it is considered the top node of the
"contents tree", that is, it is the root of the hierarchical structure
of the documents. Normally, this is "index", but if your "index"
document is a custom template, you can also set this to another filename.
> Name of your master document (without suffix) [index]: 

Sphinx can also add configuration for epub output:
> Do you want to use the epub builder (y/n) [n]: 
Indicate which of the following Sphinx extensions should be enabled:
> autodoc: automatically insert docstrings from modules (y/n) [n]: y
> doctest: automatically test code snippets in doctest blocks (y/n) [n]: y
> intersphinx: link between Sphinx documentation of different projects (y/n) [n]: n
> todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: y
> coverage: checks for documentation coverage (y/n) [n]: n
> imgmath: include math, rendered as PNG or SVG images (y/n) [n]: y
> mathjax: include math, rendered in the browser by MathJax (y/n) [n]: y
> ifconfig: conditional inclusion of content based on config values (y/n) [n]: 
> viewcode: include links to the source code of documented Python objects (y/n) [n]: y
> githubpages: create .nojekyll file to publish the document on GitHub pages (y/n) [n]: n
Note: imgmath and mathjax cannot be enabled at the same time. imgmath has been deselected.

A Makefile and a Windows command file can be generated for you so that you
only have to run e.g. `make html' instead of invoking sphinx-build
directly.
> Create Makefile? (y/n) [y]: y
> Create Windows command file? (y/n) [y]: y

Creating file ./source/conf.py.
Creating file ./source/index.rst.
Creating file ./Makefile.
Creating file ./make.bat.

Finished: An initial directory structure has been created.

```



### 3. Update config

The config file is located at `docs/source/config.py`

There are 3 parts in the config file need to update.

1. Update source code directory location

```
import os
import sys
sys.path.insert(0, os.path.abspath('../../src/'))
```

3. Import the module

```
# At the top.
import sphinx_bootstrap_theme

... 

# Activate the theme.
html_theme = 'bootstrap'
html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()

```

Ensure the extensions has been configured to use to `sphinx.ext.autodoc`

```
# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

extensions = [
    "sphinx.ext.autodoc"
]
```


3. Add html option

Edit the setting to fit your preference. You can paste the following code at the end of the config file.

```
# (Optional) Logo. Should be small enough to fit the navbar (ideally 24x24).
# Path should be relative to the ``_static`` files directory.
html_logo = "logo.png"

# Theme options are theme-specific and customize the look and feel of a
# theme further.
html_theme_options = {
    # Navigation bar title. (Default: ``project`` value)
    'navbar_title': "Demo",

    # Tab name for entire site. (Default: "Site")
    'navbar_site_name': "Site",

    # A list of tuples containing pages or urls to link to.
    # Valid tuples should be in the following forms:
    #    (name, page)                 # a link to a page
    #    (name, "/aa/bb", 1)          # a link to an arbitrary relative url
    #    (name, "http://example.com", True) # arbitrary absolute url
    # Note the "1" or "True" value above as the third argument to indicate
    # an arbitrary url.
    'navbar_links': [
        ("Examples", "examples"),
        ("Link", "http://example.com", True),
    ],

    # Render the next and previous page links in navbar. (Default: true)
    'navbar_sidebarrel': True,

    # Render the current pages TOC in the navbar. (Default: true)
    'navbar_pagenav': True,

    # Tab name for the current pages TOC. (Default: "Page")
    'navbar_pagenav_name': "Page",

    # Global TOC depth for "site" navbar tab. (Default: 1)
    # Switching to -1 shows all levels.
    'globaltoc_depth': 2,

    # Include hidden TOCs in Site navbar?
    #
    # Note: If this is "false", you cannot have mixed ``:hidden:`` and
    # non-hidden ``toctree`` directives in the same page, or else the build
    # will break.
    #
    # Values: "true" (default) or "false"
    'globaltoc_includehidden': "true",

    # HTML navbar class (Default: "navbar") to attach to <div> element.
    # For black navbar, do "navbar navbar-inverse"
    'navbar_class': "navbar navbar-inverse",

    # Fix navigation bar to top of page?
    # Values: "true" (default) or "false"
    'navbar_fixed_top': "true",

    # Location of link to source.
    # Options are "nav" (default), "footer" or anything else to exclude.
    'source_link_position': "nav",

    # Bootswatch (http://bootswatch.com/) theme.
    #
    # Options are nothing (default) or the name of a valid theme
    # such as "cosmo" or "sandstone".
    #
    # The set of valid themes depend on the version of Bootstrap
    # that's used (the next config option).
    #
    # Currently, the supported themes are:
    # - Bootstrap 2: https://bootswatch.com/2
    # - Bootstrap 3: https://bootswatch.com/3
    'bootswatch_theme': "journal",

    # Choose Bootstrap version.
    # Values: "3" (default) or "2" (in quotes)
    'bootstrap_version': "3",
}
```


#### Note
Update this option to  add CES docs link
```
    'navbar_links': [
        ("CES Document", "http://172.16.2.140:8080", True),
    ],

```

**Don’t forget to save it.**

### 4. Walk Source code and generate reStructured Text File

Firs RUN `sphinx-apidoc -o <out-dir> <-src-dir> -f`

For example, in project root folder
	`sphinx-apidoc -o docs/source/ src/` 

Then copy the toctree content in `module.rst` to `index.rst` and then remove `module.rst`


### 5. Convert reStructured Text File to HTML file

In `docs`, RUN `make html`


## Theme configuration for Sphinx


```
'bootswatch_theme':”cyborg”,
'navbar_class':”navbarnavbar-inverse”,
```

 * [GitHub - ryan-roemer/sphinx-bootstrap-theme: Sphinx Bootstrap Theme](https://github.com/ryan-roemer/sphinx-bootstrap-theme) 
 * [Sphinx Bootstrap Theme — Demo 0.7.1 documentation](https://ryan-roemer.github.io/sphinx-bootstrap-theme/README.html) 
 * [Bootswatch: Cyborg](https://bootswatch.com/3/cyborg/) 
 * [reStructuredText — Sphinx 3.0.0+/8c7faed6f documentation](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html) 
