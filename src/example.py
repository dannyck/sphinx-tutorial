class Student:

    def __init__(self, name, age):
        self.__name = name
        self.__age = age
        self.__homework = dict()

    def ask_name(self):
        print("My name is {}".format(self.__name))

    def give_homework(self, homeworks: dict):
        """

        Examples:
            .. code-block:: python

                give_homework(homeworks={
                    "hw1": "english vocab",
                    "hw2": "matrix exercise"
                })




        :param dict homeworks: homework assigned to student, in k-v format.
        :return: Return Message
        :rtype: None
        """
        for k, v in homeworks.items():
            self.__homework[k] = v

        return True

    def do_homework_now(self):
        """
        .. todo::

            This is todo notes


        .. math:: ax^2 + bx + c = 0


        Complex Table
            +----------------+--------------+---------------+
            | Second, First  |      0       |       1       |
            +================+==============+===============+
            |       0        |      1       |       d       |
            +----------------+--------------+---------------+
            |       1        |      2       |      N/A      |
            +----------------+--------------+-------+-------+
            +       a        +         b    +   c   |   d   |
            +----------------+--------------+-------+-------+

        Simple Table
            ===== ======= =======
            ColA  ColB     ColC
            ===== ======= =======
            1     2       3
            ----- ------- -------
            3     2       1
            ===== ======= =======

        Inline Markup
            | *emphasis*
            | **strong emphasis**
            | `interpreted text`
            | ``inline literals``


        List
            1. Item 1 initial text.

               a) Item 1a.
               b) Item 1b.

            2. a) Item 2a.
               b) Item 2b.

        Option
            -a         Output all.
            -b         Output both (this description is quite long).
            -c arg     Output just arg.
            --long     Output all day long.

        This is an ordinary paragraph.

        >>> print('this is a Doctest block')
        this is a Doctest block

        Admonitions
            .. hint::

                This is a hint directive!

            .. tip::

                This is a tip directive!

            .. caution::

                This is a caution directive!

        :return:
        """
        pass